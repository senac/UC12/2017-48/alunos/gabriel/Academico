package br.com.senac.academico.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Curso implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCurso")
    private int id;
    
    @Column(length = 45)
    private String nome;
    
    @JoinColumn(name = "Professor_idProfessor", referencedColumnName = "idProfessor")
    @ManyToOne(fetch = FetchType.EAGER)
    private Professor professor;
    
    @JoinTable(name = "CursoAluno",
            joinColumns = {@JoinColumn(name = "Curso_idCurso", referencedColumnName = "idCurso")},
            inverseJoinColumns = {@JoinColumn(name = "Aluno_idAluno", referencedColumnName = "idAluno")})
    @ManyToMany
    private List<Aluno> alunos;

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public List<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    } 
}
